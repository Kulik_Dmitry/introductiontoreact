﻿import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import HomePage from './HomePage'
import Login from './Login'
import Register from './Register'
import NotFount from "./NotFount";

class Navigation extends React.Component {

    render() {
        return (
            <Router>
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <Link to="/" style={LinkStyle}>Домой<span class="sr-only">(current)</span></Link>
                            </li>
                            <li class="nav-item active">
                                <Link to="/Login" style={LinkStyle}>Войти<span class="sr-only">(current)</span></Link>
                            </li>
                            <li class="nav-item active">
                                <Link to="/Register" style={LinkStyle}>Регистрация<span class="sr-only">(current)</span></Link>
                            </li>
                        </ul>
                    </div>
                </nav>

                <Switch>
                    <Route exact path="/">
                        <HomePage />
                    </Route>
                    <Route path="/Login">
                        <Login />
                    </Route>
                    <Route path="/Register">
                        <Register />
                    </Route>
                    <Route path="*">
                        <NotFount />
                    </Route>
                </Switch>
            </Router>
        );
    }
}

const LinkStyle = {
    margin: '5px'
}

export default Navigation;