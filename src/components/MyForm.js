import React from "react";
//import API from "../utils/API";
import { Form } from "shards-react";
import "bootstrap/dist/css/bootstrap.min.css";
import Input from './Input';
import Button from './Button';

class MyForm extends React.Component {
    constructor() {
        super();
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleClearForm = this.handleClearForm.bind(this);
        this.state = {
            Preference: {
                Name: ''
            }
        }

        this.handleName = this.handleName.bind(this);
    }

    handleName(e) {
        let value = e.target.value;
        this.setState(prevState => ({
            Preference:
            {
                ...prevState.Preference, Name: value
            }
        }))
    }

    async handleSubmit(event) {
        event.preventDefault();
        const response = await fetch('/Preferences/Create', {
            method: 'POST',
            body: JSON.stringify({ Name: this.state.Preference.Name }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        });
        await response;
        this.setState({
            Preference: {
                Name: ''
            },
        });
    }

    handleClearForm(e) {
        e.preventDefault();
        this.setState({
            Preference: {
                Name: ''
            },
        })
    }

    render() {
        return (
            <div className="row justify-content-center">
                <Form className="container" onSubmit={this.handleSubmit}>
                    <Input inputtype={'text'}
                        title={'Name'}
                        name={'Name'}
                        value={this.state.Preference.Name}
                        placeholder={'Enter your name'}
                        handleChange={this.handleName}
                    />

                    <Button
                        action={this.handleFormSubmit}
                        type={'primary'}
                        title={'Submit'}
                        style={buttonStyle}
                    />

                    <Button
                        action={this.handleClearForm}
                        type={'secondary'}
                        title={'Clear'}
                        style={buttonStyle}
                    />
                </Form>
            </div>
        );
    }
}
const buttonStyle = {
    margin: '10px 10px 10px 10px'
}
export default MyForm;