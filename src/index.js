import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Navigation from './RouterExample/Navigation'
import reportWebVitals from './reportWebVitals';

import { Provider } from 'react-redux';
import { store } from './configure-store';
import { Container } from './counter/container';

const App = () => (
    <React.StrictMode>
        <Navigation />
    </React.StrictMode>
);

//const App = () => (
//    <Provider store={store}>
//        <Container />
//    </Provider>
//);

ReactDOM.render(<App />, document.getElementById('root'));

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
